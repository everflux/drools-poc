package de.trion.sample.springdrools.process.auftrag;

import de.trion.sample.springdrools.model.auftrag.Auftrag;
import de.trion.sample.springdrools.model.auftrag.Zahlungsempfaenger;
import de.trion.sample.springdrools.process.base.regelprotokoll.RegelProtokoll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class GeburtsdatumValidierungTest
{
    private GeburtsdatumValidierungSchritt geburtsdatumValidierungSchritt;

    private Auftrag auftrag;
    private AuftragKontext kontext;

    @BeforeEach
    void setUp()
    {
        geburtsdatumValidierungSchritt = new GeburtsdatumValidierungSchritt();

        Zahlungsempfaenger zahlungsempfaenger = new Zahlungsempfaenger();
        auftrag = new Auftrag();
        auftrag.setZahlungsempfaenger(zahlungsempfaenger);
        kontext = new AuftragKontext(auftrag, new RegelProtokoll());
    }

    @Test
    public void pastDate()
    {
        kontext.setGeburtsdatumZahlungsempfaenger(LocalDate.now().minusYears(1));

        geburtsdatumValidierungSchritt.schrittMitRegelnAuswerten(kontext);

        assertThat(kontext.getFehlerMeldungen().isEmpty()).isTrue();
    }

    @Test
    public void futureDate()
    {
        kontext.setGeburtsdatumZahlungsempfaenger(LocalDate.now().plusYears(1));

        geburtsdatumValidierungSchritt.schrittMitRegelnAuswerten(kontext);

        assertThat(kontext.getFehlerMeldungen().isEmpty()).isFalse();
    }
}
