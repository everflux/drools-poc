package de.trion.sample.springdrools.process;

import de.trion.sample.springdrools.model.Response;
import de.trion.sample.springdrools.model.auftrag.Auftrag;
import de.trion.sample.springdrools.model.auftrag.Zahlungsempfaenger;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Integrationstest ohne Umsysteme
 */
@SpringBootTest
class AuftragServiceTest
{
    @Autowired
    AuftragService auftragService;

    @Test
    void verarbeite()
    {
        var zahlungsempfaenger = new Zahlungsempfaenger();
        zahlungsempfaenger.setGeburtsdatumZahlungsempfaenger("2023-01-19");
        var auftrag = new Auftrag();
        auftrag.setZahlungsempfaenger(zahlungsempfaenger);

        final Response response = auftragService.verarbeite(auftrag);

        assertThat(response.getFehlermeldungen().getMeldungen()).hasSize(1).contains("Geburtsdatum liegt in Zukunft");
    }
}
