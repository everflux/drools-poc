package de.trion.sample.springdrools.process.base.kontext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class FehlerMeldungen
{
    private final Logger logger = LoggerFactory.getLogger(getClass());

    List<String> meldungen = new ArrayList<>();

    public List<String> getMeldungen()
    {
        return meldungen;
    }

    public void setMeldungen(List<String> meldungen)
    {
        this.meldungen = meldungen;
    }

    public void add(String meldung)
    {
        logger.info("Fehler: {}", meldung);
        meldungen.add(meldung);
    }

    public boolean isEmpty()
    {
        return meldungen.isEmpty();
    }
}
