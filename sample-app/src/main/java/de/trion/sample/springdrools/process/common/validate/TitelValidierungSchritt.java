package de.trion.sample.springdrools.process.common.validate;

import de.trion.sample.springdrools.process.base.AbstrakterRegelSchritt;
import de.trion.sample.springdrools.process.base.kontext.AbstrakterKontext;
import org.springframework.stereotype.Component;

@Component
public class TitelValidierungSchritt extends AbstrakterRegelSchritt
{
    @Override
    protected void codeAusfuehren(AbstrakterKontext kontext)
    {
        logger.info("Validiere Titel");
    }
}
