package de.trion.sample.springdrools.process.base;

import de.trion.sample.springdrools.model.Anfrage;
import de.trion.sample.springdrools.model.Response;
import de.trion.sample.springdrools.process.base.kontext.AbstrakterKontext;
import de.trion.sample.springdrools.process.base.kontext.KontextAufbau;
import de.trion.sample.springdrools.process.base.regelprotokoll.RegelEreignis;
import de.trion.sample.springdrools.process.base.regelprotokoll.RegelProtokoll;
import de.trion.sample.springdrools.process.base.regelprotokoll.RegelProtokollEintrag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * Koordiniert den Ablauf der Regelschritte.
 * Es gibt zwei wesentliche Phasen:
 * <ul>
 *     <li>Kontext Aufbau</li>
 *     <li>Schritt Ausfuehrung</li>
 * </ul>
 */
@Service
public class SchrittKoordination
{
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final List<KontextAufbau> kontextAufbauListe;
    private final List<RegelSchritt> regelSchritte;
    private final RegelSchritt aussteuerungSchritt;

    /**
     * @param kontextAufbauListe  Setzt die Strategien zur Kontexterzeugung. Am hoechsten priorisierte Strategien sollten dabei zuerst stehen.
     * @param regelSchritte       abzuarbeitende Regelschritte fuer jeden Kontext
     * @param aussteuerungSchritt schritt zur Behandlung einer Aussteuerung
     */
    public SchrittKoordination(List<KontextAufbau> kontextAufbauListe, List<RegelSchritt> regelSchritte, @Qualifier("aussteuerung") RegelSchritt aussteuerungSchritt)
    {
        this.kontextAufbauListe = kontextAufbauListe;
        this.regelSchritte = regelSchritte;
        this.aussteuerungSchritt = aussteuerungSchritt;
    }


    /**
     * Fuehrt das Regelwerke als Abfolge von Regelschritten aus.<br>
     * Dabei wird aus der uebergebenen Anfrage eine Response erzeugt.
     * <br>
     * Im Fehlerfall wird evtl. auch eine Aussteuerung erzeugt.
     * <br>
     *
     * @param anfrage       zu verarbeitende Anfrage
     * @param correlationId Wird genutzt, um eine Zuordnung zu ermoeglichen, z.B. Dateiname und Zeilennummer
     * @return das Ergebnis der Verarbeitung.
     */
    public Response schritteAusfuehren(final Anfrage anfrage, final String correlationId)
    {
        final Response response = new Response();
        //response.setRequest(anfrage);

        final AbstrakterKontext kontext = erzeugeKontext(anfrage, correlationId);

        schritteAusfuehren(kontext);

        if (kontext.isFehlerhaft())
        {
            aussteuerungSchritt.schrittMitRegelnAuswerten(kontext);
        }

        // Endergebnis als Regelprotokoll-Eintrag festhalten
        abschlussRegelProtokollEintrag(kontext.getRegelProtokoll(), kontext.getAnfrage());

        response.setRegelprotokoll(kontext.getRegelProtokoll());
        response.setFehlermeldungen(kontext.getFehlerMeldungen());

        return response;

    }

    private void abschlussRegelProtokollEintrag(final RegelProtokoll regelProtokoll, final Anfrage anfrage)
    {
        final RegelProtokollEintrag regelProtokollEintrag = new RegelProtokollEintrag();
        regelProtokollEintrag.setProcessName("Ergebnissatz");
        regelProtokollEintrag.setRegelEreignis(RegelEreignis.JAVA_STEP);

        if (regelProtokoll == null || regelProtokoll.getRegelProtokollEintrag() == null)
        {
            logger.warn("Fehlerhafter Regelprotokollzustand!");
            return;
        }
        regelProtokoll.getRegelProtokollEintrag().add(regelProtokollEintrag);

    }


    private AbstrakterKontext erzeugeKontext(Anfrage anfrage, String correlationId)
    {
        Objects.requireNonNull(anfrage);

        RegelProtokoll regelProtokoll = new RegelProtokoll();
        for (KontextAufbau kontextAufbau : this.kontextAufbauListe)
        {
            if (kontextAufbau.akzeptiert(anfrage))
            {
                return kontextAufbau.erzeugen(anfrage, correlationId, regelProtokoll);
            }
        }
        throw new IllegalArgumentException("Keine Strategie zum Kontextaufbau konfiguriert - falscher Inputtyp? ");
    }


    private void schritteAusfuehren(final AbstrakterKontext kontext)
    {
        for (RegelSchritt schritt : regelSchritte)
        {
            schritt.schrittMitRegelnAuswerten(kontext);
            if (kontext.isFehlerhaft())
            {
                return;
            }
        }

    }
}
