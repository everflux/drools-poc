package de.trion.sample.springdrools.util;

import org.kie.api.event.rule.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingAgendaListener implements AgendaEventListener
{
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void matchCreated(MatchCreatedEvent event)
    {
        logger.info("matchCreated: {}", event.getMatch().getRule().getName());
    }

    @Override
    public void matchCancelled(MatchCancelledEvent event)
    {
        logger.info("matchCancelled: '{}' ({})", event.getMatch().getRule().getName(), event.getCause());
    }

    @Override
    public void beforeMatchFired(BeforeMatchFiredEvent event)
    {
        logger.info("beforeMatchFired: {}", event.getMatch().getRule().getName());
    }

    @Override
    public void afterMatchFired(AfterMatchFiredEvent event)
    {
        logger.info("afterMatchFired: {}", event.getMatch().getRule().getName());
    }

    @Override
    public void agendaGroupPopped(AgendaGroupPoppedEvent event)
    {
        logger.info("agendaGroupPopped: {}", event.getAgendaGroup().getName());
    }

    @Override
    public void agendaGroupPushed(AgendaGroupPushedEvent event)
    {
        logger.info("agendaGroupPushed: {}", event.getAgendaGroup().getName());
    }

    @Override
    public void beforeRuleFlowGroupActivated(RuleFlowGroupActivatedEvent event)
    {
        logger.info("beforeRuleFlowGroupActivated: {}", event);
    }

    @Override
    public void afterRuleFlowGroupActivated(RuleFlowGroupActivatedEvent event)
    {
        logger.info("afterRuleFlowGroupActivated: {}", event);
    }

    @Override
    public void beforeRuleFlowGroupDeactivated(RuleFlowGroupDeactivatedEvent event)
    {
        logger.info("beforeRuleFlowGroupDeactivated: {}", event);
    }

    @Override
    public void afterRuleFlowGroupDeactivated(RuleFlowGroupDeactivatedEvent event)
    {
        logger.info("afterRuleFlowGroupDeactivated: {}", event);
    }
}
