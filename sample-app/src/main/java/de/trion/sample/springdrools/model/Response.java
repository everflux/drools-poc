package de.trion.sample.springdrools.model;

import de.trion.sample.springdrools.process.base.kontext.FehlerMeldungen;
import de.trion.sample.springdrools.process.base.regelprotokoll.RegelProtokoll;

public class Response
{
    private RegelProtokoll regelprotokoll;
    private FehlerMeldungen fehlermeldungen;

    public void setRegelprotokoll(RegelProtokoll regelprotokoll)
    {
        this.regelprotokoll = regelprotokoll;
    }

    public RegelProtokoll getRegelprotokoll()
    {
        return regelprotokoll;
    }

    public void setFehlermeldungen(FehlerMeldungen fehlermeldungen)
    {
        this.fehlermeldungen = fehlermeldungen;
    }

    public FehlerMeldungen getFehlermeldungen()
    {
        return fehlermeldungen;
    }
}
