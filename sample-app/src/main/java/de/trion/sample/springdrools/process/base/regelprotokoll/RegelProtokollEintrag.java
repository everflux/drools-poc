package de.trion.sample.springdrools.process.base.regelprotokoll;

public class RegelProtokollEintrag
{
    private String processName;
    private RegelEreignis regelEreignis;
    private String regelName;

    public RegelProtokollEintrag()
    {
    }

    public void setProcessName(String processName)
    {
        this.processName = processName;
    }

    public String getProcessName()
    {
        return processName;
    }

    public RegelEreignis getRegelEreignis()
    {
        return regelEreignis;
    }

    public void setRegelEreignis(RegelEreignis regelEreignis)
    {
        this.regelEreignis = regelEreignis;
    }

    public void setRegelName(String regelName)
    {
        this.regelName = regelName;
    }

    public String getRegelName()
    {
        return regelName;
    }
}
