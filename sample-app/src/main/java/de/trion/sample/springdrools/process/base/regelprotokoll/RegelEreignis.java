package de.trion.sample.springdrools.process.base.regelprotokoll;

public enum RegelEreignis
{
    DROOLS, JAVA_STEP
}
