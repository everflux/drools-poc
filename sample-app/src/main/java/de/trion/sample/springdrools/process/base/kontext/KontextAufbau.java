package de.trion.sample.springdrools.process.base.kontext;

import de.trion.sample.springdrools.model.Anfrage;
import de.trion.sample.springdrools.process.base.regelprotokoll.RegelProtokoll;

/**
 * Baut einen Kontexte fuer die Verarbeitung auf
 */
public interface KontextAufbau
{
    /**
     * Liefert konkreten Kontext zur weiteren Verarbeitung. Der Typ
     * des Kontext haengt dabei von der Anfrage ab.
     *
     * @param anfrage zu verarbeitende Anfrage
     * @param correlationId correlation ID
     * @param regelProtokoll jeder Kontext erhaelt eine eigene Kopie des Regelprotokolls damit diese persistiert werden kann
     * @return mindestens ein Kontext, mehr als einer bei Splitting
     */
    AbstrakterKontext erzeugen(Anfrage anfrage, String correlationId, RegelProtokoll regelProtokoll);

    /**
     * Stellt fest ob diese Strategie fuer die Anfrage geeignet ist.
     * @param anfrage zu verwendende Anfrage
     * @return {@code true} falls diese Strategie fuer den Typ passend ist, sonst {@code false}
     */
    boolean akzeptiert(Anfrage anfrage);
}
