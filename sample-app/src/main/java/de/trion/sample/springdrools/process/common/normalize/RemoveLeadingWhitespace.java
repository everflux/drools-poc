package de.trion.sample.springdrools.process.common.normalize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Beispielannotation, entsprechender Visitor, der Bearbeitung implementiert, muesste implementiert werden
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface RemoveLeadingWhitespace
{
    public String fehlerCode() default "<nicht definiert>";
    public String fehlerMeldung() default "";
}
