package de.trion.sample.springdrools.process.auftrag;

import de.trion.sample.springdrools.model.auftrag.Auftrag;
import de.trion.sample.springdrools.process.base.kontext.AbstrakterKontext;
import de.trion.sample.springdrools.process.base.regelprotokoll.RegelProtokoll;

import java.time.LocalDate;

public class AuftragKontext extends AbstrakterKontext
{
    //aus string zu hoeherwertigem Objekt konvertiert
    private LocalDate geburtsdatumZahlungsempfaenger;

    public AuftragKontext(Auftrag auftrag, RegelProtokoll regelProtokoll)
    {
        super(auftrag, regelProtokoll);
    }


    public LocalDate getGeburtsdatumZahlungsempfaenger()
    {
        return geburtsdatumZahlungsempfaenger;
    }

    public void setGeburtsdatumZahlungsempfaenger(LocalDate geburtsdatumZahlungsempfaenger)
    {
        this.geburtsdatumZahlungsempfaenger = geburtsdatumZahlungsempfaenger;
    }
}
