package de.trion.sample.springdrools.process.base;

import de.trion.sample.springdrools.process.base.kontext.AbstrakterKontext;
import de.trion.sample.springdrools.process.base.regelprotokoll.RegelEreignis;
import de.trion.sample.springdrools.process.base.regelprotokoll.RegelProtokoll;
import de.trion.sample.springdrools.process.base.regelprotokoll.RegelProtokollAgendaListener;
import de.trion.sample.springdrools.process.base.regelprotokoll.RegelProtokollEintrag;
import de.trion.sample.springdrools.util.LoggingAgendaListener;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.internal.io.ResourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.File;
import java.io.IOException;

import static org.springframework.util.ClassUtils.classPackageAsResourcePath;


public abstract class AbstrakterRegelSchritt implements RegelSchritt
{
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    private boolean regelProtokollAktiv = true;

    private KieContainer kieContainer;

    /**
     * Aktiviert das Regelprotokoll falls {@code regelProtokollAktiv} auf true gesetzt ist. Deaktiviert das Regelprotokoll andernfalls.
     *
     * @param regelProtokollAktiv true aktiviert das Protokoll, false deaktiviert das Protokoll
     */
    public void setRegelProtokollAktiv(final boolean regelProtokollAktiv)
    {
        this.regelProtokollAktiv = regelProtokollAktiv;
    }

    //alternativ @PostConstruct
    protected AbstrakterRegelSchritt()
    {
        var resolver = new PathMatchingResourcePatternResolver(getClass().getClassLoader());

        var resolveString = "classpath*:" + classPackageAsResourcePath(getClass()) + "/" + getClass().getSimpleName() + ".drl";
        Resource[] resources = new Resource[0];
        final File drlFile;
        try
        {
            resources = resolver.getResources(resolveString);
            if (resources.length < 1)
            {
                logger.debug("Keine Regeln gefunden für {} ", resolveString);
                return;
            }
            drlFile = resources[0].getFile();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }


        var kieServices = KieServices.Factory.get();
        final KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
        kieFileSystem.write(ResourceFactory.newFileResource(drlFile));

        final KieBuilder kb = kieServices.newKieBuilder(kieFileSystem);
        kb.buildAll();

        final KieModule kieModule = kb.getKieModule();
        kieContainer = kieServices.newKieContainer(kieModule.getReleaseId());

        logger.debug("Regeln für {} gefunden", getClass().getSimpleName());
    }

    @Override
    public final void schrittMitRegelnAuswerten(final AbstrakterKontext kontext)
    {
        if (regelProtokollAktiv)
        {
            regelProtokollInitialisieren(kontext);
        }

        if (!akzeptiert(kontext))
        {
            //haelt regelprotokoll schlank, kann ggf. ueber debug log level aktiviert werden
            //regelProtokollEintrag(kontext, "Dieser Schritt verarbeitet den Kontext-Typ nicht.");
            return;
        }

        if (!vorbedingungErfuellt(kontext))
        {
            regelProtokollEintrag(kontext, "Vorbedingung fuer diesen Schritt nicht gegeben.");
            return;
        }

        datenAnreichern(kontext);

        codeAusfuehren(kontext);

        droolsRegelnAnwenden(kontext);
    }

    /**
     * Bestimmt ob dieser Schritt auf dem uebergebenen Kontext Anwendung findet, oder nicht. Falls nicht, kommt der Schritt nicht zur Ausfuehrung.
     * Liefert im Default immer {@code true} und kann ueberschrieben werden.<br>
     * <br>
     *
     * @param kontext kontext Objekt
     * @return {@code false} falls der Schritt nicht fuer den Kontext-Typ zustaendig ist, sonst {@code true}
     */
    public boolean akzeptiert(AbstrakterKontext kontext)
    {
        return true;
    }

    /**
     * Ermittelt ob die Vorbedingungen fuer den Schritt erfuellt sind. Falls nicht, kommt der Schritt nicht zur Ausfuehrung.
     * Liefert im Default immer {@code true} und kann ueberschrieben werden.<br>
     *
     * @param kontext kontext Objekt
     * @return {@code false} falls die Vorbedingungen nicht erfuellt sind, sonst {@code true}
     */
    public boolean vorbedingungErfuellt(AbstrakterKontext kontext)
    {
        return true;
    }

    /**
     * Diese Methode dient dazu fuer diesen Verarbeitungsschritt noetige Datenanreicherungen durchzufuehren.
     * Das jeweilige Verhalten wird durch Ueberschreiben vorgegeben.
     * Wird vor den zu diesem Schritt gehoerenden Regeln und Java-Code ausgefuehrt.
     *
     * @param kontext der Verarbeitungskontext
     */
    @SuppressWarnings("NoopMethodInAbstractClass")
    protected void datenAnreichern(final AbstrakterKontext kontext)
    {
        //override if required
    }

    /**
     * Diese Methode dient dazu spezielles Verhalten in Java durch Ueberschreiben vorzugeben.
     * Sie wird vor den zu diesem Schritt gehoerenden Regeln ausgefuehrt.
     *
     * @param kontext der Verarbeitungskontext
     */
    @SuppressWarnings("NoopMethodInAbstractClass")
    protected void codeAusfuehren(final AbstrakterKontext kontext)
    {
        //override if required
    }

    /**
     * Diese Methode dient dazu Drools-Regeln auszufuehren
     *
     * @param kontext der Verarbeitungskontext
     */
    @SuppressWarnings("NoopMethodInAbstractClass")
    protected void droolsRegelnAnwenden(final AbstrakterKontext kontext)
    {
        if (kieContainer == null)
        {
            return;
        }

        final KieSession kieSession = kieContainer.newKieSession();

        kieSession.addEventListener(new RegelProtokollAgendaListener(kontext.getRegelProtokoll()));

        if (logger.isDebugEnabled())
        {
            kieSession.addEventListener(new LoggingAgendaListener());
        }

        kieSession.insert(kontext);
        kieSession.insert(kontext.getFehlerMeldungen());

        weitereFacts(kieSession);

        kieSession.fireAllRules();
        kieSession.dispose();
    }

    protected void weitereFacts(KieSession kieSession)
    {

    }

    protected void regelProtokollEintrag(final AbstrakterKontext kontext, final String eintrag)
    {
        final String name = getClass().getSimpleName();

        final Logger mappedLogger;
        if (!name.isEmpty())
        {
            mappedLogger = LoggerFactory.getLogger(getClass().getName());
        }
        else
        {
            mappedLogger = logger;
        }

        mappedLogger.debug("{}", eintrag);

        if (!regelProtokollAktiv)
        {
            logger.trace("Regel Protokoll inaktiv {} ", name);
            return;
        }

        final RegelProtokollEintrag regelProtokollEintrag = new RegelProtokollEintrag();
        regelProtokollEintrag.setProcessName(name);
        regelProtokollEintrag.setRegelEreignis(RegelEreignis.JAVA_STEP);
        regelProtokollEintrag.setRegelName(eintrag);

        final RegelProtokoll regelProtokoll = kontext.getRegelProtokoll();

        if (regelProtokoll == null || regelProtokoll.getRegelProtokollEintrag() == null)
        {
            logger.warn("Fehlerhafter Regelprotokollzustand (nicht initialisiert)");
            return;
        }
        regelProtokoll.getRegelProtokollEintrag().add(regelProtokollEintrag);

    }

    private void regelProtokollInitialisieren(final AbstrakterKontext kontext)
    {
        if (kontext.getRegelProtokoll() == null)
        {
            final RegelProtokoll regelProtokoll = new RegelProtokoll();
            regelProtokoll.setName("Regelprotokoll");
            kontext.setRegelProtokoll(regelProtokoll);
        }

    }
}
