package de.trion.sample.springdrools.process.base.kontext;

import de.trion.sample.springdrools.model.Anfrage;
import de.trion.sample.springdrools.process.base.regelprotokoll.RegelProtokoll;

public abstract class AbstrakterKontextAufbau implements KontextAufbau
{
    /**
     * Bestimmt, ob ein Kontext durch diese Strategie aufgebaut werden kann, oder nicht.
     * Falls nicht, kommt der Schritt nicht zur Ausfuehrung.
     * Liefert im Default immer {@code false} und muss ueberschrieben werden.<br>
     * <br>
     *
     * @param anfrage zu verarbeitendes Objekt
     * @return {@code false} falls der Schritt nicht fuer den Kontext-Typ zustaendig ist, sonst {@code true}
     */
    public boolean akzeptiert(Anfrage anfrage)
    {
        return false;
    }

}
