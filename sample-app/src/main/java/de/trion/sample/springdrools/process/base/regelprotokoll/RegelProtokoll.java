package de.trion.sample.springdrools.process.base.regelprotokoll;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class RegelProtokoll
{
    private List<RegelProtokollEintrag> regelProtokollEintrag = new ArrayList<>();
    private String name;
    private String correlationId;
    private LocalDateTime localDateTime = LocalDateTime.now();

    public List<RegelProtokollEintrag> getRegelProtokollEintrag()
    {
        return regelProtokollEintrag;
    }

    public void setRegelProtokollEintrag(List<RegelProtokollEintrag> regelProtokollEintrag)
    {
        this.regelProtokollEintrag = regelProtokollEintrag;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setCorrelationId(String correlationId)
    {
        this.correlationId = correlationId;
    }

    public String getCorrelationId()
    {
        return correlationId;
    }

    public LocalDateTime getLocalDateTime()
    {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime)
    {
        this.localDateTime = localDateTime;
    }

    public void addEintrag(String message)
    {
        final RegelProtokollEintrag eintrag = new RegelProtokollEintrag();
        eintrag.setRegelEreignis(RegelEreignis.DROOLS);
        eintrag.setRegelName(message);

        regelProtokollEintrag.add(eintrag);
    }
}
