package de.trion.sample.springdrools.process.common;

import de.trion.sample.springdrools.process.base.AbstrakterRegelSchritt;
import de.trion.sample.springdrools.process.base.kontext.AbstrakterKontext;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Qualifier("aussteuerung")
@Component
public class AussteuerungSchritt extends AbstrakterRegelSchritt
{
    @Override
    public boolean akzeptiert(AbstrakterKontext kontext)
    {
        return kontext.isFehlerhaft();
    }

    @Override
    protected void codeAusfuehren(AbstrakterKontext kontext)
    {
        regelProtokollEintrag(kontext, "Fehlerhafter Eintrag");
    }
}
