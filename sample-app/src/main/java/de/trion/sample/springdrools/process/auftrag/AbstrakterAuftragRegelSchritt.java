package de.trion.sample.springdrools.process.auftrag;

import de.trion.sample.springdrools.process.base.kontext.AbstrakterKontext;
import de.trion.sample.springdrools.process.base.AbstrakterRegelSchritt;

public class AbstrakterAuftragRegelSchritt extends AbstrakterRegelSchritt
{
    @Override
    final public boolean akzeptiert(AbstrakterKontext kontext)
    {
        return kontext instanceof AuftragKontext;
    }
}
