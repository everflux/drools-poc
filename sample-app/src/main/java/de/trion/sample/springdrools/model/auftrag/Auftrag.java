package de.trion.sample.springdrools.model.auftrag;

import de.trion.sample.springdrools.model.Anfrage;

public class Auftrag extends Anfrage
{

    private Zahlungsempfaenger zahlungsempfaenger;

    public Zahlungsempfaenger getZahlungsempfaenger()
    {
        return zahlungsempfaenger;
    }

    public void setZahlungsempfaenger(Zahlungsempfaenger zahlungsempfaenger)
    {
        this.zahlungsempfaenger = zahlungsempfaenger;
    }
}
