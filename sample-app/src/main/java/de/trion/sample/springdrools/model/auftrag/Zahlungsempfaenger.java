package de.trion.sample.springdrools.model.auftrag;

import de.trion.sample.springdrools.process.common.normalize.RemoveLeadingWhitespace;

public class Zahlungsempfaenger
{
    @RemoveLeadingWhitespace(fehlerCode = "1234", fehlerMeldung = "Leerzeichen bei Geburtsdatum Zahlungsempfaenger entfernt.")
    private String geburtsdatumZahlungsempfaenger;

    @RemoveLeadingWhitespace
    private String anrede;

    @RemoveLeadingWhitespace
    private String titel;

    @RemoveLeadingWhitespace
    private String vorname;

    @RemoveLeadingWhitespace
    private String namenszusatz;

    @RemoveLeadingWhitespace
    private String namensvorsatz;

    @RemoveLeadingWhitespace
    private String nachname;

    public String getGeburtsdatumZahlungsempfaenger()
    {
        return geburtsdatumZahlungsempfaenger;
    }

    public void setGeburtsdatumZahlungsempfaenger(String geburtsdatumZahlungsempfaenger)
    {
        this.geburtsdatumZahlungsempfaenger = geburtsdatumZahlungsempfaenger;
    }

    public String getAnrede()
    {
        return anrede;
    }

    public void setAnrede(String anrede)
    {
        this.anrede = anrede;
    }

    public String getTitel()
    {
        return titel;
    }

    public void setTitel(String titel)
    {
        this.titel = titel;
    }

    public String getVorname()
    {
        return vorname;
    }

    public void setVorname(String vorname)
    {
        this.vorname = vorname;
    }

    public String getNamenszusatz()
    {
        return namenszusatz;
    }

    public void setNamenszusatz(String namenszusatz)
    {
        this.namenszusatz = namenszusatz;
    }

    public String getNamensvorsatz()
    {
        return namensvorsatz;
    }

    public void setNamensvorsatz(String namensvorsatz)
    {
        this.namensvorsatz = namensvorsatz;
    }

    public String getNachname()
    {
        return nachname;
    }

    public void setNachname(String nachname)
    {
        this.nachname = nachname;
    }
}
