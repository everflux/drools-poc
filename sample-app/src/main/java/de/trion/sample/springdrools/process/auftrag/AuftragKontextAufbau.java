package de.trion.sample.springdrools.process.auftrag;

import de.trion.sample.springdrools.model.Anfrage;
import de.trion.sample.springdrools.model.auftrag.Auftrag;
import de.trion.sample.springdrools.process.base.kontext.AbstrakterKontextAufbau;
import de.trion.sample.springdrools.process.base.regelprotokoll.RegelProtokoll;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * Erstellt Kontext fuer Auftrag Instanzen
 */
@Component
public class AuftragKontextAufbau extends AbstrakterKontextAufbau
{
    @Override
    public boolean akzeptiert(Anfrage anfrage)
    {
        return anfrage instanceof Auftrag;
    }

    @Override
    public AuftragKontext erzeugen(Anfrage anfrage, String correlationId, RegelProtokoll regelProtokoll)
    {
        var auftrag = (Auftrag) anfrage;

        var kontext = new AuftragKontext(auftrag, regelProtokoll);
        kontext.setCorrelationId(correlationId);

        //koennte auch wieder in schritten erfolgen
        if(auftrag.getZahlungsempfaenger().getGeburtsdatumZahlungsempfaenger() != null)
        {
            kontext.setGeburtsdatumZahlungsempfaenger(LocalDate.parse(auftrag.getZahlungsempfaenger().getGeburtsdatumZahlungsempfaenger()));
        }

        return kontext;
    }
}
