package de.trion.sample.springdrools.process.base.regelprotokoll;

import org.kie.api.event.rule.*;

public class RegelProtokollAgendaListener implements AgendaEventListener
{
    private final RegelProtokoll regelProtokoll;

    public RegelProtokollAgendaListener(RegelProtokoll regelProtokoll)
    {
        this.regelProtokoll = regelProtokoll;
    }

    @Override
    public void matchCreated(MatchCreatedEvent matchCreatedEvent)
    {

    }

    @Override
    public void matchCancelled(MatchCancelledEvent matchCancelledEvent)
    {

    }

    @Override
    public void beforeMatchFired(BeforeMatchFiredEvent beforeMatchFiredEvent)
    {

    }

    @Override
    public void afterMatchFired(AfterMatchFiredEvent afterMatchFiredEvent)
    {
        regelProtokoll.addEintrag(afterMatchFiredEvent.getMatch().getRule().getName());
    }

    @Override
    public void agendaGroupPopped(AgendaGroupPoppedEvent agendaGroupPoppedEvent)
    {

    }

    @Override
    public void agendaGroupPushed(AgendaGroupPushedEvent agendaGroupPushedEvent)
    {

    }

    @Override
    public void beforeRuleFlowGroupActivated(RuleFlowGroupActivatedEvent ruleFlowGroupActivatedEvent)
    {

    }

    @Override
    public void afterRuleFlowGroupActivated(RuleFlowGroupActivatedEvent ruleFlowGroupActivatedEvent)
    {

    }

    @Override
    public void beforeRuleFlowGroupDeactivated(RuleFlowGroupDeactivatedEvent ruleFlowGroupDeactivatedEvent)
    {

    }

    @Override
    public void afterRuleFlowGroupDeactivated(RuleFlowGroupDeactivatedEvent ruleFlowGroupDeactivatedEvent)
    {

    }
}
