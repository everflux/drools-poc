package de.trion.sample.springdrools.process.base;

import de.trion.sample.springdrools.process.base.kontext.AbstrakterKontext;

public interface RegelSchritt
{
    void schrittMitRegelnAuswerten(final AbstrakterKontext kontext);
}
