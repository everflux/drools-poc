package de.trion.sample.springdrools.process.base.kontext;

import de.trion.sample.springdrools.model.Anfrage;
import de.trion.sample.springdrools.process.base.regelprotokoll.RegelProtokoll;

import java.time.LocalDateTime;

public abstract class AbstrakterKontext
{
    private boolean fehlerhaft;
    private RegelProtokoll regelProtokoll;

    private FehlerMeldungen fehlerMeldungen;

    private Anfrage anfrage;
    private String correlationId;

    private LocalDateTime now;

    public AbstrakterKontext(Anfrage anfrage, RegelProtokoll regelProtokoll)
    {
        this.anfrage = anfrage;
        this.regelProtokoll = regelProtokoll;
        if(regelProtokoll == null)
        {
            this.regelProtokoll = new RegelProtokoll();
        }
        fehlerMeldungen = new FehlerMeldungen();
        now = LocalDateTime.now();
    }

    public boolean isFehlerhaft()
    {
        return fehlerhaft;
    }

    public void setFehlerhaft(boolean fehlerhaft)
    {
        this.fehlerhaft = fehlerhaft;
    }

    public RegelProtokoll getRegelProtokoll()
    {
        return regelProtokoll;
    }

    public void setRegelProtokoll(RegelProtokoll regelProtokoll)
    {
        this.regelProtokoll = regelProtokoll;
    }

    public Anfrage getAnfrage()
    {
        return anfrage;
    }

    public void setAnfrage(Anfrage anfrage)
    {
        this.anfrage = anfrage;
    }

    public FehlerMeldungen getFehlerMeldungen()
    {
        return fehlerMeldungen;
    }

    public void setFehlerMeldungen(FehlerMeldungen fehlerMeldungen)
    {
        this.fehlerMeldungen = fehlerMeldungen;
    }

    public void setCorrelationId(String correlationId)
    {
        this.correlationId = correlationId;
    }

    public String getCorrelationId()
    {
        return correlationId;
    }

    public LocalDateTime getNow()
    {
        return now;
    }

    public void setNow(LocalDateTime now)
    {
        this.now = now;
    }
}
