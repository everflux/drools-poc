package de.trion.sample.springdrools.process;

import de.trion.sample.springdrools.model.Response;
import de.trion.sample.springdrools.model.auftrag.Auftrag;
import de.trion.sample.springdrools.process.base.SchrittKoordination;
import org.springframework.stereotype.Service;

@Service
public class AuftragService
{
    private final SchrittKoordination schrittKoordination;


    public AuftragService(SchrittKoordination schrittKoordination)
    {
        this.schrittKoordination = schrittKoordination;
    }

    public Response verarbeite(Auftrag auftrag)
    {
        return schrittKoordination.schritteAusfuehren(auftrag, "123");
    }
}
